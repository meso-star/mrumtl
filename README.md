# ModRadUrb: MaTeriaL

This C library loads spectrally variable materials saved in mrumtl
format. See `mrumtl.5` for the format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.2

- Write the man page directly in mdoc's roff macros, instead of using
  the intermediate scdoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.1

- Complete rewrite of BRDF accessors. A BRDF is now represented by a
  single integer and not directly by a generic data structure. Thus, the
  `mrumtl_fetch_brdf` function returns an integer and not a pointer to
  the BRDF which is now returned by the new function `mrumtl_get_brdf`.
  In addition, data from a specific BRDF is now returned to a data
  structure that can therefore store more than one scalar.
- Update of the signature of the `mrumtl_create` function: the input
  arguments are now grouped into a variable of type `struct
  mrumtl_create_args`. Thanks to this structure and its default value,
  updating the input parameters should now marginally affect the calling
  code.
- Use scdoc rather than asciidoc as file format for man sources.

### Version 0.0.1

- Bump the CMake minimum version to 3.1. Previously it was set to 2.8
  which is deprecated.

## Licenses

Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)

MruMtl is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute them under certain
conditions; refer to the COPYING file for details.
