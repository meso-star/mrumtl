# Copyright (C) 2020, 2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libmrumtl.a
LIBNAME_SHARED = libmrumtl.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/mrumtl.c src/mrumtl_log.c src/mrumtl_fetch.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS)

$(LIBNAME_STATIC): libmrumtl.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libmrumtl.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -DMRUMTL_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    mrumtl.pc.in > mrumtl.pc

mrumtl-local.pc: mrumtl.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    mrumtl.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" mrumtl.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include" src/mrumtl.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/mrumtl" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" mrumtl.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/mrumtl.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/mrumtl/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/mrumtl/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/mrumtl.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/mrumtl.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libmrumtl.o mrumtl.pc mrumtl-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh
	mandoc -Tlint -Wall mrumtl.5

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_mrumtl.c\
 src/test_mrumtl_bands.c\
 src/test_mrumtl_wlens.c\
 src/test_mrumtl_load.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
MRUMTL_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags mrumtl-local.pc)
MRUMTL_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs mrumtl-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)
	rm -f test_file_band.mrumtl

$(TEST_DEP): config.mk mrumtl-local.pc
	@$(CC) $(CFLAGS_EXE) $(MRUMTL_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk mrumtl-local.pc
	$(CC) $(CFLAGS_EXE) $(MRUMTL_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_mrumtl \
test_mrumtl_bands \
test_mrumtl_wlens \
test_mrumtl_load \
: config.mk mrumtl-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(MRUMTL_LIBS) $(RSYS_LIBS)
