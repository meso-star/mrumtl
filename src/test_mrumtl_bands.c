/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "mrumtl.h"
#include <rsys/mem_allocator.h>

#include <stdio.h>

static void
test_load1(struct mrumtl* mrumtl)
{
  FILE* fp = NULL;
  const char* filename = "test_file_band.mrumtl";
  const struct mrumtl_brdf* brdf = NULL;
  struct mrumtl_brdf_lambertian lambertian = MRUMTL_BRDF_LAMBERTIAN_NULL;
  struct mrumtl_brdf_specular specular = MRUMTL_BRDF_SPECULAR_NULL;

  CHK(fp = fopen(filename, "w+"));
  fprintf(fp, "# Comment\n");
  fprintf(fp, "bands 2 # Comment at the end of a line\n");
  fprintf(fp, "200.1 280.3 lambertian 1\n");
  fprintf(fp, "381 700.4 specular 0.4\n");
  rewind(fp);

  CHK(mrumtl_load_stream(NULL, fp, filename) == RES_BAD_ARG);
  CHK(mrumtl_load_stream(mrumtl, NULL, filename) == RES_BAD_ARG);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);

  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, filename) == RES_OK);

  CHK(mrumtl_load(NULL, filename) == RES_BAD_ARG);
  CHK(mrumtl_load(mrumtl, NULL) == RES_BAD_ARG);
  CHK(mrumtl_load(mrumtl, "invalid") == RES_IO_ERR);
  CHK(mrumtl_load(mrumtl, filename) == RES_OK);

  CHK(mrumtl_get_brdfs_count(mrumtl) == 2);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 0));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);

  CHK(mrumtl_brdf_get_lambertian(NULL, &lambertian) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_lambertian(brdf, NULL) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 200.1);
  CHK(lambertian.wavelengths[1] == 280.3);
  CHK(lambertian.reflectivity == 1);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 1));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);

  CHK(mrumtl_brdf_get_specular(NULL, &specular) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_specular(brdf, NULL) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 381);
  CHK(specular.wavelengths[1] == 700.4);
  CHK(specular.reflectivity == 0.4);

  CHK(fclose(fp) == 0);
}

static void
test_load2(struct mrumtl* mrumtl)
{
  struct mrumtl_brdf_lambertian lambertian = MRUMTL_BRDF_LAMBERTIAN_NULL;
  struct mrumtl_brdf_specular specular = MRUMTL_BRDF_SPECULAR_NULL;
  const struct mrumtl_brdf* brdf = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  fprintf(fp, "bands 6\n");
  fprintf(fp, "200.0 200.0 lambertian 1\n");
  fprintf(fp, "200.1 200.2 specular 0\n");
  fprintf(fp, "200.2 200.5 specular 0.3\n");
  fprintf(fp, "300.4 480.2 specular 0.6\n");
  fprintf(fp, "500.0 501.0 lambertian 0.5\n");
  fprintf(fp, "680.6 780.7 specular 0.63\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);

  CHK(mrumtl_get_brdfs_count(mrumtl) == 6);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 0));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 200.0 && lambertian.wavelengths[1] == 200.0);
  CHK(lambertian.reflectivity == 1);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 1));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_BAD_ARG);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 200.1 && specular.wavelengths[1] == 200.2);
  CHK(specular.reflectivity == 0);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 2));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 200.2 && specular.wavelengths[1] == 200.5);
  CHK(specular.reflectivity == 0.3);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 3));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 300.4 && specular.wavelengths[1] == 480.2);
  CHK(specular.reflectivity == 0.6);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 4));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 500.0 && lambertian.wavelengths[1] == 501.0);
  CHK(lambertian.reflectivity == 0.5);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 5));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 680.6 && specular.wavelengths[1] == 780.7);
  CHK(specular.reflectivity == 0.63);

  fclose(fp);
}

static void
test_fetch(struct mrumtl* mrumtl)
{
  FILE* fp = NULL;
  size_t ibrdf = 0;

  CHK(fp = tmpfile());
  fprintf(fp, "bands 0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(mrumtl_fetch_brdf(NULL, 400, 0.5, &ibrdf) == RES_BAD_ARG);
  CHK(mrumtl_fetch_brdf(mrumtl, -1, 0.5, &ibrdf) == RES_BAD_ARG);
  CHK(mrumtl_fetch_brdf(mrumtl, 400, -0.1, &ibrdf) == RES_BAD_ARG);
  CHK(mrumtl_fetch_brdf(mrumtl, 400, 1, &ibrdf) == RES_BAD_ARG);
  CHK(mrumtl_fetch_brdf(mrumtl, 400, 0.5, NULL) == RES_BAD_ARG);
  CHK(mrumtl_fetch_brdf(mrumtl, 400, 0.5, &ibrdf) == RES_OK);
  CHK(ibrdf >= mrumtl_get_brdfs_count(mrumtl));

  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "200 300 lambertian 0.0\n");
  fprintf(fp, "300 400 lambertian 0.1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(mrumtl_fetch_brdf(mrumtl, 100, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 450, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 300, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 300.1, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);

  CHK(fp = tmpfile());
  fprintf(fp, "bands 4\n");
  fprintf(fp, "100 150 lambertian 0\n");
  fprintf(fp, "200 200 lambertian 1\n");
  fprintf(fp, "300 400 lambertian 0.1\n");
  fprintf(fp, "400 401 specular 0.5\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(mrumtl_fetch_brdf(mrumtl, 160, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 160, 0.5, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 160, 0.8, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 200, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 250, 0.49, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 250, 0.5, &ibrdf) == RES_OK);
  CHK(ibrdf == 2);
  CHK(mrumtl_fetch_brdf(mrumtl, 400.1, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 3);
}

static void
test_load_fail(struct mrumtl* mrumtl)
{
  FILE* fp = NULL;

  /* Invalid keyword */
  CHK(fp = tmpfile());
  fprintf(fp, "bande 1\n");
  fprintf(fp, "200.0 200.1 lambertian 1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* No band count */
  CHK(fp = tmpfile());
  fprintf(fp, "bands\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a band definition */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "200.0 200.1 lambertian 1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid wavelengths */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "-200.0 200.1 lambertian 1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a band boundary */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200.0 lambertian 1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid BRDF */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200.0 200.1 Lambertian 1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid reflectivity */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200.0 200.1 lambertian -0.1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid reflectivity */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200.1 200.2 specular 1.1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);


  /* Additional parameters. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1 additional_text\n");
  fprintf(fp, "200.0 200.1 lambertian 0.1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  /* Unsorted BRDFs */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "200.1 200.2 specular 0\n");
  fprintf(fp, "200.0 200.1 lambertian 0.1\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* BRDFs overlap */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "200.0 200.2 lambertian 0.1\n");
  fprintf(fp, "200.1 200.3 specular 0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200 300");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200 300 lambertian");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200 300 specular");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional parameters. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "200 300 specular 1 additional_text");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
}

int
main(int argc, char** argv)
{
  struct mrumtl_create_args args = MRUMTL_CREATE_ARGS_DEFAULT;
  struct mrumtl* mrumtl = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);
  CHK(mrumtl_get_brdfs_count(mrumtl) == 0);

  test_load1(mrumtl);
  test_load2(mrumtl);
  test_fetch(mrumtl);
  test_load_fail(mrumtl);

  CHK(mrumtl_ref_put(mrumtl) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
