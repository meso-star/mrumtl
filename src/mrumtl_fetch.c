/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "mrumtl.h"
#include "mrumtl_c.h"
#include "mrumtl_log.h"
#include "mrumtl_brdf.h"

#include <rsys/algorithm.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
cmp_brdf(const void* key, const void* item)
{
  const double wavelength = (*(const double*)key);
  const struct mrumtl_brdf* brdf = item;
  if(wavelength < brdf->wlen[0]) {
    return -1;
  } else if(wavelength > brdf->wlen[1]) {
    return 1;
  } else {
    return 0;
  }
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
mrumtl_fetch_brdf
  (const struct mrumtl* mrumtl,
   const double wavelength,
   const double sample, /* In [0, 1[ */
   size_t* out_ibrdf)
{
  const struct mrumtl_brdf* brdfs = NULL;
  const struct mrumtl_brdf* brdf = NULL;
  size_t nbrdfs = 0;
  size_t ibrdf = 0;
  res_T res = RES_OK;

  if(!mrumtl
  || wavelength < 0
  || sample < 0
  || sample >= 1
  || !out_ibrdf) {
    res = RES_BAD_ARG;
    goto error;
  }

  brdfs = darray_brdf_cdata_get(&mrumtl->brdfs);
  nbrdfs = darray_brdf_size_get(&mrumtl->brdfs);

  if(!nbrdfs) {
    log_warn(mrumtl, "%s: no BRDF to fetch.\n", FUNC_NAME);
    *out_ibrdf = 0;
    goto exit;
  }

  brdf = search_lower_bound
    (&wavelength, brdfs, nbrdfs, sizeof(*brdfs), cmp_brdf);

  /* All BRDFs are set for a wavelength greater or equal to the submitted
   * wavelength */
  if(brdf == brdfs) {
    ASSERT(brdfs[0].wlen[0] > wavelength 
       || (brdfs[0].wlen[0] <= wavelength && wavelength <= brdfs[0].wlen[1]));
    ibrdf = 0;
  }

  /* All BRDFs are set for a wavelength less than the submitted wavelength */
  else if(!brdf) {
    ASSERT(brdfs[nbrdfs-1].wlen[1] < wavelength);
    ibrdf = nbrdfs - 1;
  }

  /* The wavelength range of the found brdf function overlaps the submitted
   * wavelength */
  else if(wavelength >= brdf->wlen[0] && wavelength <= brdf->wlen[1]) {
    ibrdf = (size_t)(brdf - brdfs);
  }

  /* The found BRDF is defined for a wavelength greater or equal to the
   * submitted wavelength */
  else {
    /* Compute the linear interpolation parameter that defines the submitted
     * wavelength wrt to the wavelenths boundaries of the BRDF surrounding it*/
    const struct mrumtl_brdf* brdf0 = brdf-1;
    const struct mrumtl_brdf* brdf1 = brdf;
    const double u =
      (brdf1->wlen[0] - wavelength)
    / (brdf1->wlen[0] - brdf0->wlen[1]);

    /* Consider the linear interplation parameter previously defined as a
     * probability and use the submitted sample to select one of the BRDFs
     * surrounding the submitted wavelength. */
    if(sample < u) {
      ibrdf = (size_t)(brdf - brdfs - 1);
    } else {
      ibrdf = (size_t)(brdf - brdfs);
    }
  }

  ASSERT(ibrdf < nbrdfs);
  *out_ibrdf = ibrdf;

exit:
  return res;
error:
  goto exit;
}
