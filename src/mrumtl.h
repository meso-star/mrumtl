/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef MRUMTL_H
#define MRUMTL_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(MRUMTL_SHARED_BUILD) /* Build shared library */
  #define MRUMTL_API extern EXPORT_SYM
#elif defined(MRUMTL_STATIC) /* Use/build static library */
  #define MRUMTL_API extern LOCAL_SYM
#else /* Use shared library */
  #define MRUMTL_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the mrumtl function `Func'
 * returns an error. One should use this macro on mrumtl function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define MRUMTL(Func) ASSERT(mrumtl_ ## Func == RES_OK)
#else
  #define MRUMTL(Func) mrumtl_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

enum mrumtl_brdf_type {
  MRUMTL_BRDF_LAMBERTIAN,
  MRUMTL_BRDF_SPECULAR,
  MRUMTL_BRDF_NONE__
};

struct mrumtl_create_args {
  struct logger* logger; /* NULL <=> use default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define MRUMTL_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct mrumtl_create_args MRUMTL_CREATE_ARGS_DEFAULT =
  MRUMTL_CREATE_ARGS_DEFAULT__;

/* Specular BRDF */
struct mrumtl_brdf_specular {
  double wavelengths[2]; /* Inclusive wavelength range in nanometers */
  double reflectivity;
};
#define MRUMTL_BRDF_SPECULAR_NULL__ {0}
static const struct mrumtl_brdf_specular MRUMTL_BRDF_SPECULAR_NULL =
  MRUMTL_BRDF_SPECULAR_NULL__;

/* Lambertian BRDF */
struct mrumtl_brdf_lambertian {
  double wavelengths[2]; /* Inclusive wavelength range in nanometers */
  double reflectivity;
};
#define MRUMTL_BRDF_LAMBERTIAN_NULL__ {0}
static const struct mrumtl_brdf_lambertian MRUMTL_BRDF_LAMBERTIAN_NULL =
  MRUMTL_BRDF_LAMBERTIAN_NULL__;

/* Forward declaration of opaque data types */
struct mrumtl;
struct mrumtl_brdf;

BEGIN_DECLS

/*******************************************************************************
 * MRUMTL API
 ******************************************************************************/
MRUMTL_API res_T
mrumtl_create
  (const struct mrumtl_create_args* args,
   struct mrumtl** mrumtl);

MRUMTL_API res_T
mrumtl_ref_get
  (struct mrumtl* mrumtl);

MRUMTL_API res_T
mrumtl_ref_put
  (struct mrumtl* mrumtl);

MRUMTL_API res_T /* Clean up the previously loaded material */
mrumtl_load
  (struct mrumtl* mrumtl,
   const char* path);

MRUMTL_API res_T /* Clean up the previously created material */
mrumtl_load_stream
  (struct mrumtl* mrumtl,
   FILE* stream,
   const char* stream_name); /* May be NULL */

MRUMTL_API size_t
mrumtl_get_brdfs_count
  (const struct mrumtl* mrumtl);

MRUMTL_API const struct mrumtl_brdf*
mrumtl_get_brdf
  (const struct mrumtl* mrumtl,
   const size_t ibrdf);

MRUMTL_API enum mrumtl_brdf_type
mrumtl_brdf_get_type
  (const struct mrumtl_brdf* brdf);

MRUMTL_API res_T
mrumtl_brdf_get_lambertian
  (const struct mrumtl_brdf* brdf,
   struct mrumtl_brdf_lambertian* lambertian);

MRUMTL_API res_T
mrumtl_brdf_get_specular
  (const struct mrumtl_brdf* brdf,
   struct mrumtl_brdf_specular* specular);

/* Fetch a BRDF with respect to the submitted wavelength and the given sample.
 * The BRDF is chosen according to the alpha coefficient between [0, 1] defined
 * as below:
 *
 *    alpha = (lambda1 - wavelength) / (lambda1 - lambda0)
 *
 * with lambda0 < lambda1, the BRDF wavelengths surrounding the submitted
 * wavelength. The index returned refers to the BRDF corresponding to lambda0
 * or lambda1 depending on whether the given sample is less than or greater
 * than alpha, respectively.
 *
 * If the BRDF are stored by spectral band, lambda0 and lambda1 are the upper
 * boundary of the lower band and the lower boundary of the upper band,
 * respectively, which surround the wavelength. If the wavelength is in a
 * specific band, the index for that band is simply returned.
 *
 * If the wavelength is outside the domain, the index returned refers to either
 * the first or the last BRDF depending on whether the wavelength is below or
 * above the domain, respectively. */
MRUMTL_API res_T
mrumtl_fetch_brdf
  (const struct mrumtl* mrumtl,
   const double wavelength,
   const double sample, /* In [0, 1[ */
   size_t* ibrdf);

END_DECLS

#endif /* MRUMTL_H */

