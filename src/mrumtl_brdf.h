/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef MRUMTL_BRDF_H
#define MRUMTL_BRDF_H

#include "mrumtl.h"
#include <rsys/dynamic_array.h>

struct mem_allocator;

struct brdf_lambertian {
  double reflectivity;
};

struct brdf_specular {
  double reflectivity;
};

struct mrumtl_brdf {
  double wlen[2]; /* Inclusive spectral range in nanometers */
  enum mrumtl_brdf_type type;
  union {
    struct brdf_lambertian lambertian;
    struct brdf_specular specular;
  } param;
};

static INLINE res_T
brdf_init(struct mem_allocator* allocator, struct mrumtl_brdf* brdf)
{
  ASSERT(brdf);
  (void)allocator;
  brdf->wlen[0] = 0;
  brdf->wlen[1] = 0;
  brdf->type = MRUMTL_BRDF_NONE__;
  return RES_OK;
}

/* Generate the dynamic array of BRDF */
#define DARRAY_NAME brdf
#define DARRAY_DATA struct mrumtl_brdf
#define DARRAY_FUNCTOR_INIT brdf_init
#include <rsys/dynamic_array.h>

#endif /* MRUMTL_BRDF_H */
