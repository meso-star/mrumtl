/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "mrumtl.h"
#include <rsys/mem_allocator.h>

#include <stdio.h>

static void
test_load(struct mrumtl* mrumtl)
{
  struct mrumtl_brdf_specular specular = MRUMTL_BRDF_SPECULAR_NULL;
  struct mrumtl_brdf_lambertian lambertian = MRUMTL_BRDF_LAMBERTIAN_NULL;
  const struct mrumtl_brdf* brdf = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(mrumtl_get_brdfs_count(mrumtl) == 0);
  CHK(fclose(fp) == 0);

  CHK(fp = tmpfile());
  fprintf(fp, "# Comment\n");
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "\n");
  fprintf(fp, "200.1 lambertian 0\n");
  rewind(fp);

  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);

  CHK(mrumtl_get_brdfs_count(mrumtl) == 1);
  CHK(brdf = mrumtl_get_brdf(mrumtl, 0));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);

  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 200.1);
  CHK(lambertian.wavelengths[1] == 200.1);
  CHK(lambertian.reflectivity == 0);
  CHK(fclose(fp) == 0);

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 10\n");
  fprintf(fp, "\n");
  fprintf(fp, "# Short waves\n");
  fprintf(fp, "430     specular    0.02\n");
  fprintf(fp, "450     specular    0.05\n");
  fprintf(fp, "750\t   specular    0.9\n");
  fprintf(fp, "# Long waves\n");
  fprintf(fp, "1100    lambertian  0.1\n");
  fprintf(fp, "1300    lambertian  0.57\n");
  fprintf(fp, "1400    lambertian  0.4\n");
  fprintf(fp, "2100    lambertian  0.3\n");
  fprintf(fp, "2500    lambertian\t0.9\n");
  fprintf(fp, "2900    lambertian  0.4\n");
  fprintf(fp, "100000  lambertian  0.0\n");
  rewind(fp);

  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(mrumtl_get_brdfs_count(mrumtl) == 10);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 0));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 430);
  CHK(specular.wavelengths[1] == 430);
  CHK(specular.reflectivity == 0.02);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 1));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 450);
  CHK(specular.wavelengths[1] == 450);
  CHK(specular.reflectivity == 0.05);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 2));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_SPECULAR);
  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);
  CHK(specular.wavelengths[0] == 750);
  CHK(specular.wavelengths[1] == 750);
  CHK(specular.reflectivity == 0.9);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 3));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 1100);
  CHK(lambertian.wavelengths[1] == 1100);
  CHK(lambertian.reflectivity == 0.1);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 4));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 1300);
  CHK(lambertian.wavelengths[1] == 1300);
  CHK(lambertian.reflectivity == 0.57);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 5));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 1400);
  CHK(lambertian.wavelengths[1] == 1400);
  CHK(lambertian.reflectivity == 0.4);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 6));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 2100);
  CHK(lambertian.wavelengths[1] == 2100);
  CHK(lambertian.reflectivity == 0.3);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 7));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 2500);
  CHK(lambertian.wavelengths[1] == 2500);
  CHK(lambertian.reflectivity == 0.9);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 8));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 2900);
  CHK(lambertian.wavelengths[1] == 2900);
  CHK(lambertian.reflectivity == 0.4);

  CHK(brdf = mrumtl_get_brdf(mrumtl, 9));
  CHK(mrumtl_brdf_get_type(brdf) == MRUMTL_BRDF_LAMBERTIAN);
  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);
  CHK(lambertian.wavelengths[0] == 100000);
  CHK(lambertian.wavelengths[1] == 100000);
  CHK(lambertian.reflectivity == 0);

  CHK(fclose(fp) == 0);
}

static void
test_fetch(struct mrumtl* mrumtl)
{
  FILE* fp = NULL;
  size_t ibrdf = 0;

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 400, 0.5, &ibrdf) == RES_OK);
  CHK(ibrdf >= mrumtl_get_brdfs_count(mrumtl));

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "200 lambertian 0\n");
  fprintf(fp, "300 lambertian 0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(mrumtl_fetch_brdf(mrumtl, 299, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 301, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 200, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 300, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 280, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 280, 0.19, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 280, 0.20, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 4\n");
  fprintf(fp, "100 lambertian 0\n");
  fprintf(fp, "200 lambertian 1\n");
  fprintf(fp, "300 lambertian 0.5\n");
  fprintf(fp, "400 specular 0.4\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(mrumtl_fetch_brdf(mrumtl, 50, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 0);
  CHK(mrumtl_fetch_brdf(mrumtl, 120, 0.8, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 270, 0.29, &ibrdf) == RES_OK);
  CHK(ibrdf == 1);
  CHK(mrumtl_fetch_brdf(mrumtl, 270, 0.3, &ibrdf) == RES_OK);
  CHK(ibrdf == 2);
  CHK(mrumtl_fetch_brdf(mrumtl, 350, 0.5, &ibrdf) == RES_OK);
  CHK(ibrdf == 3);
  CHK(mrumtl_fetch_brdf(mrumtl, 450, 0, &ibrdf) == RES_OK);
  CHK(ibrdf == 3);
}

static void
test_load_fail(struct mrumtl* mrumtl)
{
  FILE* fp = NULL;

  /* No wavelength count */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths\n");
  fprintf(fp, "380 lambertian 0.5\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a phase function */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "380 lambertian 0.5\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid wavelength */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "-380 lambertian 0.5\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Unsorted phase functions */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "380 lambertian 0.5\n");
  fprintf(fp, "280 lambertian 0.0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Phase functions overlap */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "280 lambertian 0.5\n");
  fprintf(fp, "280 lambertian 0.0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional text. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2 additional_text\n");
  fprintf(fp, "280 lambertian 0.5\n");
  fprintf(fp, "380 lambertian 0.0\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "280\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing data */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "280 lambertian\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional text. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "280 lambertian 1 additional_text\n");
  rewind(fp);
  CHK(mrumtl_load_stream(mrumtl, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
}

int
main(int argc, char** argv)
{
  struct mrumtl_create_args args = MRUMTL_CREATE_ARGS_DEFAULT;
  struct mrumtl* mrumtl = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);
  CHK(mrumtl_get_brdfs_count(mrumtl) == RES_OK);

  test_load(mrumtl);
  test_fetch(mrumtl);
  test_load_fail(mrumtl);

  CHK(mrumtl_ref_put(mrumtl) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
