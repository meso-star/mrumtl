/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r support */

#include "mrumtl.h"
#include "mrumtl_c.h"
#include "mrumtl_log.h"

#include <rsys/algorithm.h>
#include <rsys/cstr.h>
#include <rsys/text_reader.h>

#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_mrumtl_create_args(const struct mrumtl_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static res_T
parse_brdf_lambertian
  (struct mrumtl* mrumtl,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct brdf_lambertian* brdf)
{
  char* tk = NULL;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && tk_ctx && brdf);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: missing lambertian reflectivity.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &brdf->reflectivity);
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid lambertian reflectivity `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  if(brdf->reflectivity < 0 || brdf->reflectivity > 1) {
    log_err(mrumtl, "%s:%lu: the lambertian reflectivity must be in [0, 1] "
      "while the submitted value is %g.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      brdf->reflectivity);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(mrumtl, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_brdf_specular
  (struct mrumtl* mrumtl,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct brdf_specular* brdf)
{
  char* tk = NULL;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && tk_ctx && brdf);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: missing specular reflectivity.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &brdf->reflectivity);
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid specular reflectivity `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  if(brdf->reflectivity < 0 || brdf->reflectivity > 1) {
    log_err(mrumtl, "%s:%lu: the specular reflectivity must be in [0, 1] "
      "while the submitted value is %g.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      brdf->reflectivity);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(mrumtl, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_brdf
  (struct mrumtl* mrumtl,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct mrumtl_brdf* brdf)
{
  char* tk = NULL;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && tk_ctx && brdf);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: missing BRDF type.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  if(!strcmp(tk, "lambertian")) {
    brdf->type = MRUMTL_BRDF_LAMBERTIAN;
    res = parse_brdf_lambertian(mrumtl, txtrdr, tk_ctx, &brdf->param.lambertian);
    if(res != RES_OK) goto error;

  } else if(!strcmp(tk, "specular")) {
    brdf->type = MRUMTL_BRDF_SPECULAR;
    res = parse_brdf_specular(mrumtl, txtrdr, tk_ctx, &brdf->param.specular);
    if(res != RES_OK) goto error;

  } else {
    log_err(mrumtl, "%s:%lu: invalid BRDF type `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_wlen_brdf
  (struct mrumtl* mrumtl,
   struct txtrdr* txtrdr,
   struct mrumtl_brdf* brdf)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  double wlen = 0;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && brdf);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(mrumtl, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    const size_t nexpect = darray_brdf_size_get(&mrumtl->brdfs);
    const size_t nparsed = (size_t)
      (brdf - darray_brdf_cdata_get(&mrumtl->brdfs));
    log_err(mrumtl,
      "%s:%lu: missing BRDF. "
      "Expecting %lu BRDF%swhile %lu %s parsed.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      (unsigned long)nexpect, nexpect == 1 ? " " : "s ",
      (unsigned long)nparsed, nparsed > 1 ? "were" : "was");
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_double(tk, &wlen);
  if(res == RES_OK && wlen < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid wavelength `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }
  brdf->wlen[0] = wlen;
  brdf->wlen[1] = wlen;

  res = parse_brdf(mrumtl, txtrdr, &tk_ctx, brdf);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_band_brdf
  (struct mrumtl* mrumtl,
   struct txtrdr* txtrdr,
   struct mrumtl_brdf* brdf)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && brdf);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(mrumtl, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    const size_t nexpect = darray_brdf_size_get(&mrumtl->brdfs);
    const size_t nparsed = (size_t)
      (brdf - darray_brdf_cdata_get(&mrumtl->brdfs));
    log_err(mrumtl,
      "%s:%lu: missing BRDF. "
      "Expecting %lu BRDF%swhile %lu %s parsed.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      (unsigned long)nexpect, nexpect == 1 ? " " : "s ",
      (unsigned long)nparsed, nparsed > 1 ? "were" : "was");
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_double(tk, &brdf->wlen[0]);
  if(res == RES_OK && brdf->wlen[0] < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid band min boundary `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: missing band max boundary.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &brdf->wlen[1]);
  if(res == RES_OK && brdf->wlen[1] < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid band max boundary `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  if(brdf->wlen[0] > brdf->wlen[1]) {
    log_err(mrumtl, "%s:%lu: band boundaries are degenerated -- [%g, %g].\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      brdf->wlen[0], brdf->wlen[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  res = parse_brdf(mrumtl, txtrdr, &tk_ctx, brdf);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_per_wlen_brdf(struct mrumtl* mrumtl, struct txtrdr* txtrdr, char** tk_ctx)
{
  char* tk = NULL;
  unsigned long count = 0;
  unsigned long i;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && tk_ctx);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: the number of wavelengths is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_ulong(tk, &count);
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid number of wavelengths `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(mrumtl, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  res = darray_brdf_resize(&mrumtl->brdfs, count);
  if(res != RES_OK) {
    log_err(mrumtl,
      "%s:%lu: could not allocate the list of BRDFs -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  FOR_EACH(i, 0, count) {
    struct mrumtl_brdf* brdf = NULL;

    brdf = darray_brdf_data_get(&mrumtl->brdfs) + i;

    res = parse_wlen_brdf(mrumtl, txtrdr, brdf);
    if(res != RES_OK) goto error;
    ASSERT(brdf->wlen[0] == brdf->wlen[1]);

    if(i > 0 && brdf[0].wlen[0] <= brdf[-1].wlen[0]) {
      log_err(mrumtl,
        "%s:%lu: the BRDFs must be sorted un ascending order of wavelengths "
        "(brdf %lu at %g nm; brdf %lu at %g nm).\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
        (unsigned long)(i-1), brdf[-1].wlen[0],
        (unsigned long)(i),   brdf[ 0].wlen[0]);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  darray_brdf_clear(&mrumtl->brdfs);
  goto exit;
}

static res_T
parse_per_band_brdf(struct mrumtl* mrumtl, struct txtrdr* txtrdr, char** tk_ctx)
{
  char* tk = NULL;
  unsigned long count = 0;
  unsigned long i = 0;
  res_T res = RES_OK;
  ASSERT(mrumtl && txtrdr && tk_ctx);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(mrumtl, "%s:%lu: the number of bands is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_ulong(tk, &count);
  if(res != RES_OK) {
    log_err(mrumtl, "%s:%lu: invalid number of bands `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(mrumtl, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  res = darray_brdf_resize(&mrumtl->brdfs, count);
  if(res != RES_OK) {
    log_err(mrumtl,
      "%s:%lu: could not allocate the list of BRDFs -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  FOR_EACH(i, 0, count) {
    struct mrumtl_brdf* brdf = NULL;

    brdf = darray_brdf_data_get(&mrumtl->brdfs) + i;

    res = parse_band_brdf(mrumtl, txtrdr, brdf);
    if(res != RES_OK) goto error;

    if(i > 0) {
      if(brdf[0].wlen[0] <  brdf[-1].wlen[1]
      || brdf[0].wlen[0] == brdf[-1].wlen[0]) {
        log_err(mrumtl,
          "%s:%lu: the BRDFs must be sorted in ascending order "
          "of bands and must not overlap (BRDF %lu in [%g, %g] nm; "
          "BRDF %lu in [%g %g] nm)\n",
          txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
          (unsigned long)(i-1), brdf[-1].wlen[0], brdf[-1].wlen[1],
          (unsigned long)(i),   brdf[ 0].wlen[0], brdf[ 0].wlen[1]);
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }

exit:
  return res;
error:
  darray_brdf_clear(&mrumtl->brdfs);
  goto exit;
}

static INLINE void
clear_mrumtl(struct mrumtl* mrumtl)
{
  ASSERT(mrumtl);
  str_clear(&mrumtl->name);
  darray_brdf_clear(&mrumtl->brdfs);
}

static res_T
load_stream
  (struct mrumtl* mrumtl,
   FILE* stream,
   const char* stream_name)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  struct txtrdr* txtrdr = NULL;
  res_T res = RES_OK;
  ASSERT(mrumtl && stream && stream_name);

  clear_mrumtl(mrumtl);

  res = txtrdr_stream(mrumtl->allocator, stream, stream_name, '#', &txtrdr);
  if(res != RES_OK) {
    log_err(mrumtl, "Could not create the text reader to parse `%s' -- %s.\n",
      stream_name, res_to_cstr(res));
    goto error;
  }

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(mrumtl, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  res = str_set(&mrumtl->name, stream_name);
  if(res != RES_OK) {
    log_err(mrumtl, "Could not copy the stream name `%s' -- %s.\n",
      stream_name, res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) goto exit; /* No line to parse */

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  if(!strcmp(tk, "wavelengths")) {
    res = parse_per_wlen_brdf(mrumtl, txtrdr, &tk_ctx);
    if(res != RES_BAD_ARG) goto error;
  } else if(!strcmp(tk, "bands")) {
    res = parse_per_band_brdf(mrumtl, txtrdr, &tk_ctx);
    if(res != RES_BAD_ARG) goto error;
  } else {
    log_err(mrumtl, "%s:%lu: invalid directive `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  return res;
error:
  goto exit;
}

static void
release_mrumtl(ref_T* ref)
{
  struct mrumtl* mrumtl;
  ASSERT(ref);
  mrumtl = CONTAINER_OF(ref, struct mrumtl, ref);

  darray_brdf_release(&mrumtl->brdfs);
  str_release(&mrumtl->name);

  /* First release the programs that uses the libs array */
  if(mrumtl->logger == &mrumtl->logger__) logger_release(&mrumtl->logger__);
  MEM_RM(mrumtl->allocator, mrumtl);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
mrumtl_create
  (const struct mrumtl_create_args* args,
   struct mrumtl** out_mrumtl)
{
  struct mrumtl* mrumtl = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_mrumtl) { res = RES_BAD_ARG;  goto error; }
  res = check_mrumtl_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  mrumtl = MEM_CALLOC(allocator, 1, sizeof(*mrumtl));
  if(!mrumtl) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the MruMtl handler.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&mrumtl->ref);
  mrumtl->allocator = allocator;
  mrumtl->verbose = args->verbose;
  darray_brdf_init(mrumtl->allocator, &mrumtl->brdfs);
  str_init(mrumtl->allocator, &mrumtl->name);

  if(args->logger) {
    mrumtl->logger = args->logger;
  } else {
    res = setup_log_default(mrumtl);
    if(res != RES_OK) {
      if(args->verbose) {
        fprintf(stderr, MSG_ERROR_PREFIX
          "Could not setup the MruMtl logger.\n");
      }
      goto error;
    }
    mrumtl->logger = &mrumtl->logger__;
  }

exit:
  if(out_mrumtl) *out_mrumtl = mrumtl;
  return res;
error:
  if(mrumtl) {
    MRUMTL(ref_put(mrumtl));
    mrumtl = NULL;
  }
  goto exit;
}

res_T
mrumtl_ref_get(struct mrumtl* mrumtl)
{
  if(!mrumtl) return RES_BAD_ARG;
  ref_get(&mrumtl->ref);
  return RES_OK;
}

res_T
mrumtl_ref_put(struct mrumtl* mrumtl)
{
  if(!mrumtl) return RES_BAD_ARG;
  ref_put(&mrumtl->ref, release_mrumtl);
  return RES_OK;
}

res_T
mrumtl_load(struct mrumtl* mrumtl, const char* filename)
{
  FILE* fp = NULL;
  res_T res = RES_OK;

  if(!mrumtl || !filename) {
    res = RES_BAD_ARG;
    goto error;
  }

  fp = fopen(filename, "r");
  if(!fp) {
    log_err(mrumtl, "%s: error opening file `%s' -- %s.\n",
      FUNC_NAME, filename, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(mrumtl, fp, filename);
  if(res != RES_OK) goto error;

exit:
  if(fp) fclose(fp);
  return res;
error:
  goto exit;
}

res_T
mrumtl_load_stream
  (struct mrumtl* mrumtl,
   FILE* stream,
   const char* stream_name)
{
  res_T res = RES_OK;

  if(!mrumtl || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = load_stream(mrumtl, stream, stream_name ? stream_name : "<stream>");
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

size_t
mrumtl_get_brdfs_count(const struct mrumtl* mrumtl)
{
  ASSERT(mrumtl);
  return darray_brdf_size_get(&mrumtl->brdfs);
}

const struct mrumtl_brdf*
mrumtl_get_brdf(const struct mrumtl* mrumtl, const size_t ibrdf)
{
  ASSERT(mrumtl && ibrdf < mrumtl_get_brdfs_count(mrumtl));
  return darray_brdf_cdata_get(&mrumtl->brdfs) + ibrdf;
}

enum mrumtl_brdf_type
mrumtl_brdf_get_type(const struct mrumtl_brdf* brdf)
{
  ASSERT(brdf);
  return brdf->type;
}

res_T
mrumtl_brdf_get_lambertian
  (const struct mrumtl_brdf* brdf,
   struct mrumtl_brdf_lambertian* lambertian)
{
  if(!brdf || !lambertian) return RES_BAD_ARG;
  if(brdf->type != MRUMTL_BRDF_LAMBERTIAN) return RES_BAD_ARG;
  lambertian->wavelengths[0] = brdf->wlen[0];
  lambertian->wavelengths[1] = brdf->wlen[1];
  lambertian->reflectivity = brdf->param.lambertian.reflectivity;
  return RES_OK;
} 

res_T
mrumtl_brdf_get_specular
  (const struct mrumtl_brdf* brdf,
   struct mrumtl_brdf_specular* specular)
{
  if(!brdf || !specular) return RES_BAD_ARG;
  if(brdf->type != MRUMTL_BRDF_SPECULAR) return RES_BAD_ARG;
  specular->wavelengths[0] = brdf->wlen[0];
  specular->wavelengths[1] = brdf->wlen[1];
  specular->reflectivity = brdf->param.specular.reflectivity;
  return RES_OK;
}

