/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "mrumtl.h"
#include <rsys/mem_allocator.h>

static void
check_brdf_lambertian(const struct mrumtl_brdf* brdf)
{
  struct mrumtl_brdf_lambertian lambertian = MRUMTL_BRDF_LAMBERTIAN_NULL;

  CHK(mrumtl_brdf_get_lambertian(brdf, &lambertian) == RES_OK);

  /* !NaN */
  CHK(lambertian.wavelengths[0] == lambertian.wavelengths[0]);
  CHK(lambertian.wavelengths[1] == lambertian.wavelengths[1]);
  CHK(lambertian.reflectivity == lambertian.reflectivity);

  CHK(lambertian.wavelengths[0] <= lambertian.wavelengths[1]);
  CHK(0 <= lambertian.reflectivity && lambertian.reflectivity <= 1);
}


static void
check_brdf_specular(const struct mrumtl_brdf* brdf)
{
  struct mrumtl_brdf_specular specular = MRUMTL_BRDF_SPECULAR_NULL;

  CHK(mrumtl_brdf_get_specular(brdf, &specular) == RES_OK);

  /* !NaN */
  CHK(specular.wavelengths[0] == specular.wavelengths[0]);
  CHK(specular.wavelengths[1] == specular.wavelengths[1]);
  CHK(specular.reflectivity == specular.reflectivity);

  CHK(specular.wavelengths[0] <= specular.wavelengths[1]);
  CHK(0 <= specular.reflectivity && specular.reflectivity <= 1);
}

int
main(int argc, char** argv)
{
  struct mrumtl_create_args args = MRUMTL_CREATE_ARGS_DEFAULT;
  struct mrumtl* mrumtl = NULL;
  int i;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);

  FOR_EACH(i, 1, argc) {
    size_t ibrdf;
    size_t nbrdfs;

    printf("Load %s\n", argv[i]);
    CHK(mrumtl_load(mrumtl, argv[i]) == RES_OK);

    nbrdfs = mrumtl_get_brdfs_count(mrumtl);
    FOR_EACH(ibrdf, 0, nbrdfs) {
      const struct mrumtl_brdf* brdf = mrumtl_get_brdf(mrumtl, ibrdf);

      switch(mrumtl_brdf_get_type(brdf)) {
        case MRUMTL_BRDF_LAMBERTIAN:
          check_brdf_lambertian(brdf);
          break;
        case MRUMTL_BRDF_SPECULAR:
          check_brdf_specular(brdf);
          break;
        default: FATAL("Unreachable code\n"); break;
      }
    }
  }
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
