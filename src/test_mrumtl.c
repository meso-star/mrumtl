/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "mrumtl.h"
#include <rsys/logger.h>

static void
log_stream(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)msg, (void)ctx;
  printf("%s", msg);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct logger logger;
  struct mrumtl_create_args args = MRUMTL_CREATE_ARGS_DEFAULT;
  struct mrumtl* mrumtl;
  (void)argc, (void)argv;

  CHK(mrumtl_create(NULL, &mrumtl) == RES_BAD_ARG);
  CHK(mrumtl_create(&args, NULL) == RES_BAD_ARG);
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);

  CHK(mrumtl_ref_get(NULL) == RES_BAD_ARG);
  CHK(mrumtl_ref_get(mrumtl) == RES_OK);
  CHK(mrumtl_ref_put(NULL) == RES_BAD_ARG);
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  args.allocator = &allocator;
  args.verbose = 1;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);

  CHK(logger_init(&mem_default_allocator, &logger) == RES_OK);
  logger_set_stream(&logger, LOG_OUTPUT, log_stream, NULL);
  logger_set_stream(&logger, LOG_ERROR, log_stream, NULL);
  logger_set_stream(&logger, LOG_WARNING, log_stream, NULL);

  args.logger = &logger;
  args.verbose = 0;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);
  args.allocator = NULL;
  CHK(mrumtl_create(&args, &mrumtl) == RES_OK);
  CHK(mrumtl_ref_put(mrumtl) == RES_OK);

  mem_shutdown_proxy_allocator(&allocator);
  logger_release(&logger);
  CHK(mem_allocated_size() == 0);
  return 0;
}
